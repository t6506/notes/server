package org.example.framework.server.router;

import org.example.framework.server.exception.AmbiguousMappingException;
import org.example.framework.server.http.HttpMethod;
import org.example.framework.server.util.Pair;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodRouter {
    private final Map<Pattern, Map<HttpMethod, Pair<Object, Method>>> routes;

    public MethodRouter() {
        routes = new HashMap<>();
    }

    private MethodRouter(final Map<Pattern, Map<HttpMethod, Pair<Object, Method>>> routes) {
        this.routes = routes;
    }

    public synchronized void register(final HttpMethod httpMethod, final Pattern pattern, final Object bean, final Method beanMethod) {
        Class<?> clazz = bean.getClass();
        Map<HttpMethod, Pair<Object, Method>> httpMethodsPairMap = routes.computeIfAbsent(pattern, o -> new HashMap<>());
        if (httpMethodsPairMap.get(httpMethod) != null) {
            throw new AmbiguousMappingException(clazz.getName(), beanMethod.getName(), pattern.pattern());
        }
        httpMethodsPairMap.put(httpMethod, new Pair<>(bean, beanMethod));
    }

    public static MethodRouter of(final Map<Pattern, Map<HttpMethod, Pair<Object, Method>>> routes) {
        return new MethodRouter(routes);
    }

    public synchronized Optional<MethodRoute> findController(final String requestPath, final String requestMethod) {
        for (Map.Entry<Pattern, Map<HttpMethod, Pair<Object, Method>>> entry : routes.entrySet()) {
            final Matcher matcher = entry.getKey().matcher(requestPath);
            if (!matcher.matches()) {
                continue;
            }

            for (Map.Entry<HttpMethod, Pair<Object, Method>> controllers : entry.getValue().entrySet()) {
                HttpMethod httpMethod = controllers.getKey();
                Object controller = controllers.getValue().getFirst();
                Method method = controllers.getValue().getSecond();
                if (httpMethod.name().equals(requestMethod)) {
                    return Optional.of(MethodRoute.builder()
                            .bean(controller)
                            .beanMethod(method)
                            .matcher(matcher)
                            .build());
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        final MethodRouter that = (MethodRouter) other;

        if (routes == null) {
            return that.routes == null;
        }
        if (routes.size() != that.routes.size()) {
            return false;
        }

        for (final Map.Entry<Pattern, ?> entry : routes.entrySet()) {
            final String pattern = entry.getKey().pattern();
            final Object expectedPatternRoutes = entry.getValue();
            final Object actualPatternRoutes = that.routes.entrySet().stream()
                    .filter(o -> o.getKey().pattern().equals(pattern))
                    .findFirst()
                    .map(Map.Entry::getValue)
                    .orElse(null);
            if (!Objects.equals(expectedPatternRoutes, actualPatternRoutes)) {
                return false;
            }
        }
        return true;
    }


    @Override
    public int hashCode() {
        return routes != null ? routes.hashCode() : 0;
    }
}
