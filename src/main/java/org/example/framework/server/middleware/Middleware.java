package org.example.framework.server.middleware;

import org.example.framework.server.http.Request;

import java.net.Socket;

public interface Middleware {
  void handle(final Socket socket, final Request request);
}
