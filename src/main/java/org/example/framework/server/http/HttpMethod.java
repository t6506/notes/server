package org.example.framework.server.http;

public enum HttpMethod {
  GET,
  POST,
  PUT,
  PATCH,
  DELETE;
}
