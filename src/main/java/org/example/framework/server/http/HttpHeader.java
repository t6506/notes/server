package org.example.framework.server.http;

public enum HttpHeader {
  CONTENT_TYPE("Content-Type"),
  CONTENT_LENGTH("Content-Length"),
  ACCEPT("Accept"),
  AUTHORIZATION("Authorization");

  private final String value;

  HttpHeader(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }
}
