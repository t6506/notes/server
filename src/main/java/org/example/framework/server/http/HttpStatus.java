package org.example.framework.server.http;

public enum HttpStatus {
  S200("200 Ok"),
  S404("404 Not Found"),
  S405("405 Method Not Allowed"),
  S500("500 Internal Server Error");

  private final String value;

  HttpStatus(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }
}
