package org.example.framework.server.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lists {
    private Lists() {
    }

    public static <E> List<E> of() {
        return Collections.emptyList();
    }

    public static <E> List<E> of(E element1) {
        return Collections.singletonList(element1);
    }

    public static <E> List<E> of(E element1, E element2) {
        final List<E> list = new ArrayList<>(of(element1));
        list.add(element2);
        return list;
    }

    public static <E> List<E> of(E element1, E element2, E element3) {
        final List<E> list = new ArrayList<>(of(element1, element2));
        list.add(element3);
        return list;
    }
}
