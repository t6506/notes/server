package org.example.framework.server.util;


import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class Pair<K, V> {
    private final K first;
    private final V second;
}
