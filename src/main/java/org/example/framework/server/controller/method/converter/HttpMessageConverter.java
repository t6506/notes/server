package org.example.framework.server.controller.method.converter;

import org.example.framework.server.http.MediaType;
import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

public interface HttpMessageConverter {
    boolean canRead(final Class<?> clazz, final MediaType mediaType);
    boolean canWrite(final Class<?> clazz, final MediaType mediaType);

    Object read(final Class<?> clazz, final Request request);
    
    void write(final Object object, final MediaType mediaType, final Response response);


}
