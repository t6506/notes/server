package org.example.framework.server.controller.method.resolver;

import org.example.framework.server.http.Request;

import java.lang.reflect.Parameter;

public interface ArgumentResolver {
    boolean supportParameter(final Parameter parameter);
    Object resolveArgument(final Parameter parameter, final Request request) throws Exception;
}
