package org.example.framework.server.controller.method.handler;

import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;

public interface ReturnValueHandler {
    boolean supportReturnType(final Method method);
    void handleReturnType(final Object returnValue, final Method method, final Request request, Response response)
            throws Exception;
}
