package org.example.framework.server.handler;

import org.example.framework.server.http.HttpStatus;
import org.example.framework.server.http.MediaType;
import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@FunctionalInterface
public interface Handler {
  void handle(final Request request, final Response response) throws IOException;

  static void internalServerError(final Request request, final Response response) {
    final byte[] body = "Something bad happened".getBytes(StandardCharsets.UTF_8);
    response.writeResponse(HttpStatus.S500, MediaType.TEXT_PLAIN, body);
  }

  static void notFoundHandler(final Request request, final Response response) {
    final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
    response.writeResponse(HttpStatus.S404, MediaType.TEXT_PLAIN, body);
  }

  static void methodNotAllowedHandler(final Request request, final Response response) {
    final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
    response.writeResponse(HttpStatus.S405, MediaType.TEXT_PLAIN, body);
  }
}
