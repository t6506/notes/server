package org.example.framework.server.exception;

public class UnsupportedParameterTypeException extends RuntimeException {
    public UnsupportedParameterTypeException() {
    }

    public UnsupportedParameterTypeException(final String message) {
        super(message);
    }

    public UnsupportedParameterTypeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnsupportedParameterTypeException(final Throwable cause) {
        super(cause);
    }

    public UnsupportedParameterTypeException(final String message, final Throwable cause, final boolean enableSuppression,
                                             final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
