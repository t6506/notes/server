package org.example.framework.server.exception;

public class BadAuthenticationException extends Exception {
    public BadAuthenticationException() {
        super();
    }

    public BadAuthenticationException(String message) {
        super(message);
    }

    public BadAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadAuthenticationException(Throwable cause) {
        super(cause);
    }

    protected BadAuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
