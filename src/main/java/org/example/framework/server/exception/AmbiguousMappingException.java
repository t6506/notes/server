package org.example.framework.server.exception;

public class AmbiguousMappingException extends RuntimeException {
    public AmbiguousMappingException(final String className, final String methodName, final String path) {
        super("try to map controller " + className + " method " + methodName + " to path " + path);
    }

    public AmbiguousMappingException(final String message) {
        super(message);
    }

    public AmbiguousMappingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AmbiguousMappingException(final Throwable cause) {
        super(cause);
    }

    public AmbiguousMappingException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
