package org.example.framework.server.exception;

public class ResponseWriteException extends RuntimeException {
    public ResponseWriteException() {
    }

    public ResponseWriteException(final String message) {
        super(message);
    }

    public ResponseWriteException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ResponseWriteException(final Throwable cause) {
        super(cause);
    }

    public ResponseWriteException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
