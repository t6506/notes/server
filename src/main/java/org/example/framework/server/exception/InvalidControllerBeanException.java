package org.example.framework.server.exception;

public class InvalidControllerBeanException extends RuntimeException {
    public InvalidControllerBeanException() {
    }

    public InvalidControllerBeanException(final String message) {
        super(message);
    }

    public InvalidControllerBeanException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidControllerBeanException(final Throwable cause) {
        super(cause);
    }

    public InvalidControllerBeanException(final String message, final Throwable cause, final boolean enableSuppression,
                                          final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
